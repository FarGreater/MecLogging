// MEC-Logging.h
/*
 * ****************************************************************************
 * 
 * Libary for Logging activity on the ESP2866
 * 
 * ****************************************************************************
 * By Michael Egtved Christensen 2017-2019
 * ****************************************************************************
 * 
 * Debug libarry, for sending debug information to serialport and
 * to MQTT channel (if activated)
 * 
 *   info				-> sends debug text to both serial port and MQTT if below set debug level.
 *   infonl			-> same as above with newline on serial port
 *  ActivityLED -> Turns the ActivityLED on/off "ramdomly" isch
 *   setup 			-> include in main setup
 *  loop				-> include loop in main loop
 *
 *  INTERNAL:
 *
 *  NOTE:
 *
 *  For usage see reference implementation
 * 
 * ****************************************************************************
 */
#include "Arduino.h"
#include "MecLogging.h"

#define LogLength 20
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//
// MecLogging
//
//
////////////////////////////////////////////////////////////////////////////////
MecLogging::MecLogging(int level, int ActivityLedPin, int ActivityLedMode)
{

  Serial.begin(9600);
  delay(50);
  Serial.println(".");
  Serial.println(".");
  Serial.println(".");
  delay(50);
  Serial.println(".");
  Serial.println(".");
  Serial.println(".");
  _LogLevel = level;
  _alivetimer = millis();
  _ActivityLedPin = ActivityLedPin;
  _ActivityLedMode = ActivityLedMode;
  pinMode(ActivityLedPin, OUTPUT);
  for (int i = 0; i < LogLength; i++)
  {
    _LogStringArray[i] = "";
  };

  _LogStringArray[0] = "";
  infoNl("MecLogging: Object Created ready for logging", Inf);
};

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//
// setup
//
//
////////////////////////////////////////////////////////////////////////////////
void MecLogging::setup()
{
  infoNl("MecLogging: Setup complete", Inf);
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//
// loop
//
//
////////////////////////////////////////////////////////////////////////////////
void MecLogging::loop()
{

  // send et keepAlivePing med faste intervaller
  if (millis() > _alivetimer + alivepulse)
  {

    infoNl("MecLogging: KeepAlivePing", Sto);

    _alivetimer = millis();
  }
}

////////////////////////////////////////
// Info()
// Skriver tekst til serielporten...
////////////////////////////////////////
void MecLogging::infoAppend(String text, int level)
{

  ActivityLED(LedActivity);

  if (level <= _LogLevel)
  {
    Serial.print(text);

    _LogStringArray[_LogPtr % LogLength] = _LogStringArray[_LogPtr % LogLength] + text;
  }
}

void MecLogging::infoNl(String text, int level)
{

  ActivityLED(LedActivity);

  if (level <= _LogLevel)
  {
    text = "Level:" + String(level, DEC) + " Message: " + text;
    Serial.println(text);

    if (text.length() != 0)
    {
      _LogStringArray[_LogPtr % LogLength] = text;
      _LogPtr = _LogPtr + 1;
      if (_LogPtr > 40)
      {
        _LogPtr % LogLength + LogLength;
      };
    }
  };
};

/*
 * ***************************************************************************
 *  
 * 
 * ***************************************************************************
 */

String MecLogging::GetLogArray()
{
  String Svar = "";
  for (int i = _LogPtr; i < _LogPtr + LogLength; i++)
  {
    Svar = Svar + "\n" + String(i) + "    " + _LogStringArray[i % LogLength];
  }

  return Svar;
}
////////////////////////////////////////////////////////////////////////////////
// ActivityLED
//
////////////////////////////////////////////////////////////////////////////////
void MecLogging::ActivityLED(int command)
{

  if (_ActivityLedMode != None)
  {
    switch (command)
    {
    case LedON:
      //Serial.println("LedON LOW");
      //digitalWrite(_ActivityLedPin, LOW);
      if (_ActivityLedMode == Normal)
      {
        digitalWrite(_ActivityLedPin, HIGH);
      }
      else
      {
        digitalWrite(_ActivityLedPin, LOW);
      }
      _ActivityLedStatus = LedON;
      break;
    case LedActivity:
      if (_ActivityLedStatus == LedOFF)
      {
        //Serial.println("LedActivity LOW");
        //digitalWrite(_ActivityLedPin, LOW);
        if (_ActivityLedMode == Normal)
        {
          digitalWrite(_ActivityLedPin, HIGH);
        }
        else
        {
          digitalWrite(_ActivityLedPin, LOW);
        }
        _ActivityLedStatus = LedON;
        break;
      }
      if (_ActivityLedStatus == LedON)
      {
        //Serial.println("LedActivity HIGH");
        //digitalWrite(_ActivityLedPin, HIGH);
        if (_ActivityLedMode == Normal)
        {
          digitalWrite(_ActivityLedPin, LOW);
        }
        else
        {
          digitalWrite(_ActivityLedPin, HIGH);
        }

        _ActivityLedStatus = LedOFF;
      }
      break;
    case LedOFF:
    default:
      //Serial.println("LedOFF HIGH");
      //digitalWrite(_ActivityLedPin, HIGH);
      if (_ActivityLedMode == Normal)
      {
        digitalWrite(_ActivityLedPin, LOW);
      }
      else
      {
        digitalWrite(_ActivityLedPin, HIGH);
      }

      _ActivityLedStatus = 99;
    }
  }
}