# MEC-Logging

First of 3 parts of the MEC library package.

MEC-Logging is the first, most basic libary class. The purpose of the libary is to

* List logs on serial port
* Save Log lines in an array, to be displayed on a web page
* Flash the a led if there is some kind of activity

The library has the following methods:

```c++
  MecLogging(int level, int ActivityLedPin, int ActivityLedMode); // initier objektet, med debug level
  void setup();
  void loop();
  void infoAppend(String text, int level); // skriv debug information, uden newline
  void infoNl(String text, int level);     // skriv debug information, med newline
  void ActivityLED(int command);           // blinkende aktivitetsLED
  String GetLogArray();                    // Hent seneste logs
```

## Documentation

mylogging.setup() and mylogging.loop() to be called from main program setup and loop.

### MecLogging
```c++
  MecLogging(int level, int ActivityLedPin, int ActivityLedMode); 
```

<b>level:</b>
What level of log's lines to show:
```c++
//  debug levels
#define Sto 1 // Emergency (sto)			System is unusable.
#define Err 2 // Errors (err)             	Error conditions.
#define War 3 // Warnings (warn)        	Warning conditions
#define Inf 4 // Information (info)      	Informational messages
#define Deb 5 // Debug (debug)          	Debug information
#define Lib 6 // Debug in Framework
```

<b>ActivityLedPin:</b>
The Led for flashing

<b>ActivityLedMode:</b>
```c++
#define Inverse -1
#define Normal 1
#define None 0
```

Note: use mode None for no flashing of the led.

### infoAppend / infoNl

```c++
  void infoAppend(String text, int level); 
  void infoNl(String text, int level);     
```
infoNl, writes a logline, but it is only shown if the level is higher than the set level at opbjec creation.

### ActivityLED

changes status for the flashing led. LedActivity changes status (on to off / off to on)

```c++
#define LedOFF 1
#define LedON 2
#define LedActivity 3
```

