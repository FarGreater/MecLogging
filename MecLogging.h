/*
 * ****************************************************************************
 * 
 * Libary for Logging activity on the ESP2866
 * 
 * ****************************************************************************
 * By Michael Egtved Christensen 2017-2019
 * ****************************************************************************
 * 
 * Log libarry, for storing and sharing Logging and debugginginformation to serialport and
 * to web  (if activated)
 * 
 *   info				-> sends debug text to both serial port and MQTT if below set debug level.
 *   infonl			-> same as above with newline on serial port
 *  ActivityLED -> Turns the ActivityLED on/off "ramdomly" isch
 *   setup 			-> include in main setup
 *  loop				-> include loop in main loop
 *
 *  INTERNAL:
 *
 *  NOTE:
 *
 *  For usage see reference implementation
 * 
 * ****************************************************************************
 */
#ifndef MecLogging_h
#define MecLogging_h

#include "Arduino.h"

// Pinout for the NodeMCU
#define D0 16
#define D1 5
#define D2 4
#define D3 0
#define D4 2
#define D5 14
#define D6 12
#define D7 13
#define D8 15
#define D9 3
#define D10 1
#define A0 0

//  debug levels
#define Sto 1 // Emergency (sto)			System is unusable.
#define Err 2 // Errors (err)             	Error conditions.
#define War 3 // Warnings (warn)        	Warning conditions
#define Inf 4 // Information (info)      	Informational messages
#define Deb 5 // Debug (debug)          	Debug information
#define Lib 6 // Debug in Framework

#define Inverse -1
#define Normal 1
#define None 0

// for flashy workload LED
#define LedOFF 1
#define LedON 2
#define LedActivity 3

// time interval for alivepulse
#define alivepulse 10 * 60 * 1000

class MecLogging
{
public:
  MecLogging(int level, int ActivityLedPin, int ActivityLedMode); // initier objektet, med debug level

  void setup();
  void loop();
  void infoAppend(String text, int level); // skriv debug information, uden newline
  void infoNl(String text, int level);     // skriv debug information, med newline
  void ActivityLED(int command);           // blinkende aktivitetsLED
  String GetLogArray();                    // Hent seneste logs

private:
  int _LogLevel;
  int _alivetimer;
  int _ActivityLedPin = 16;
  int _ActivityLedStatus = 99;
  int _ActivityLedMode;
  String _LogStringArray[20];
  int _LogPtr = 0;
};

#endif
